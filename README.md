# Money Transfer

Simple service to simulate money transfer between accounts. 

Used libraries are:
* Blade
* Lombok
* JUnit
 
### Running 
You can start service by running:

```
$ mvn clean package
$ java -jar target/money-transfer-1.0-jar-with-dependencies.jar
```

### API

Create new account
```
REQUEST
[POST] http://localhost:9000/account
{
	"owner": "Alper Tekinalp",
	"balance": 200
}

RESPONSE
{
    "payload": {
        "accountId": 1,
        "owner": "Alper Tekinalp",
        "balance": "200.00"
    },
    "success": true,
    "msg": null,
    "code": 0,
    "timestamp": 1581028459
}
```

Get an existing account:
```
REQUEST
[GET] http://localhost:9000/account/:accountId

RESPONSE
{
    "payload": {
        "accountId": 1,
        "owner": "Alper Tekinalp",
        "balance": "200.00"
    },
    "success": true,
    "msg": null,
    "code": 0,
    "timestamp": 1581028459
}
```

Deposit money to an existing account:
```
REQUEST
[POST] http://localhost:9000/deposit
{
	"accountId": 1,
	"amount": 500
}

RESPONSE
{
    "payload": {
        "transactionId": "DEP-265cfa57-e96d-4005-9faf-dec45f39b423",
        "result": {
            "status": "RECEIVED",
            "message": null
        },
        "accountId": 1,
        "amount": "500.00"
    },
    "success": true,
    "msg": null,
    "code": 0,
    "timestamp": 1581028997
}
```

Withdraw money from an existing account:
```
REQUEST
[POST] http://localhost:9000/withdraw
{
	"accountId": 1,
	"amount": 500
}

RESPONSE
{
    "payload": {
        "accountId": 1,
        "amount": "500.00",
        "transactionId": "WTD-3a608cd8-e991-433c-b0d0-5eec98f230af",
        "result": {
            "status": "RECEIVED",
            "message": null
        }
    },
    "success": true,
    "msg": null,
    "code": 0,
    "timestamp": 1581029064
}
```

Transfer money between accounts:
```
REQUEST
[POST] http://localhost:9000/transfer
{
	"senderId": 1,
	"receiverId": 2,
	"balance": 200
}

RESPONSE
{
    "payload": {
        "transactionId": "TRX-f61ca4b6-6859-41e5-a8f3-1b8b1da44642",
        "result": {
            "status": "RECEIVED",
            "message": null
        },
        "senderId": 1,
        "receiverId": 2,
        "amount": "200.00"
    },
    "success": true,
    "msg": null,
    "code": 0,
    "timestamp": 1581103173
}
```
Operations with money transfer (`deposit`, `withdraw` and `transfer`) works asynchronously. They return a transaction
 id and current status of the transaction. You can control latest state with:
 
 ```
REQUEST
[GET] http://localhost:9000/transaction/:transactionId

RESPONSE
{
    "payload": {
        "transactionId": "TRX-f61ca4b6-6859-41e5-a8f3-1b8b1da44642",
        "result": {
            "status": "SUCCESS",
            "message": null
        },
        "senderId": 1,
        "receiverId": 2,
        "amount": "200.00"
    },
    "success": true,
    "msg": null,
    "code": 0,
    "timestamp": 1581104338
}
``` 