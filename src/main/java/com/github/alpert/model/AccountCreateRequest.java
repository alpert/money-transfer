package com.github.alpert.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountCreateRequest {
    private String owner;
    private BigDecimal balance;
}
