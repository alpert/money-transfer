package com.github.alpert.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Deposit {
    Long accountId;
    BigDecimal amount;
}
