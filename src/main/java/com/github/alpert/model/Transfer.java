package com.github.alpert.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Transfer {
    Long senderId;
    Long receiverId;
    BigDecimal amount;
}
