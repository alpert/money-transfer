package com.github.alpert.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Withdraw {
    Long accountId;
    BigDecimal amount;
}
