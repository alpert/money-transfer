package com.github.alpert.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class Account {
    private Long accountId;
    private String owner;
    private BigDecimal balance;
}
