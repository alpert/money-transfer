package com.github.alpert.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TransactionResult {
    TransactionStatus status;
    String message;

    public TransactionResult setStatus(final TransactionStatus status) {
        this.status = status;
        return this;
    }
}
