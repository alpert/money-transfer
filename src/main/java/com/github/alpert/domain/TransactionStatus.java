package com.github.alpert.domain;

public enum TransactionStatus {
    RECEIVED, SUCCESS, FAILURE
}
