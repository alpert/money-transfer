package com.github.alpert.persistence;

import com.blade.ioc.annotation.Bean;
import com.github.alpert.domain.Account;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Bean
public class AccountStore {
    private final Map<Long, Account> accounts = new ConcurrentHashMap<>();
    private final AtomicLong idGenerator = new AtomicLong();

    public Account save(final Account account) {
        if (account.getAccountId() == null) {
            account.setAccountId(idGenerator.incrementAndGet());
        }

        accounts.put(account.getAccountId(), account);
        return account;
    }

    public Account get(final Long accountId) {
        return accounts.get(accountId);
    }

    public void clear() {
        accounts.clear();
    }
}
