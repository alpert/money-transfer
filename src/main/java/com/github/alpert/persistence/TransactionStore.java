package com.github.alpert.persistence;

import com.blade.ioc.annotation.Bean;
import com.github.alpert.transaction.Transaction;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Bean
public class TransactionStore {
    private final Map<String, Transaction> transactions = new ConcurrentHashMap<>();

    public void save(final Transaction transactionResult) {
        transactions.put(transactionResult.getTransactionId(), transactionResult);
    }

    public void update(final Transaction transactionResult) {
        transactions.put(transactionResult.getTransactionId(), transactionResult);
    }

    public Transaction getById(final String transactionId) {
        return transactions.get(transactionId);
    }
}
