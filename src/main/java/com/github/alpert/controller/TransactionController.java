package com.github.alpert.controller;

import com.blade.ioc.annotation.Inject;
import com.blade.mvc.annotation.GetRoute;
import com.blade.mvc.annotation.JSON;
import com.blade.mvc.annotation.Path;
import com.blade.mvc.annotation.PathParam;
import com.blade.mvc.ui.RestResponse;
import com.github.alpert.transaction.Transaction;
import com.github.alpert.service.TransactionService;
import io.netty.handler.codec.http.HttpResponseStatus;

@Path
public class TransactionController {
    @Inject
    private TransactionService transactionService;

    @GetRoute("transaction/:transactionId")
    @JSON
    public RestResponse<?> getTransaction(@PathParam final String transactionId) {
        final Transaction transactionEvent = transactionService.get(transactionId);
        if (transactionEvent == null) {
            return RestResponse
                .fail(HttpResponseStatus.NOT_FOUND.code(),
                    "Could not found transaction with id: " + transactionId);
        }

        return RestResponse.ok(transactionEvent);
    }
}
