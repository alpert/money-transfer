package com.github.alpert.controller;

import com.blade.ioc.annotation.Inject;
import com.blade.mvc.annotation.BodyParam;
import com.blade.mvc.annotation.GetRoute;
import com.blade.mvc.annotation.JSON;
import com.blade.mvc.annotation.Path;
import com.blade.mvc.annotation.PathParam;
import com.blade.mvc.annotation.PostRoute;
import com.blade.mvc.ui.RestResponse;
import com.github.alpert.domain.Account;
import com.github.alpert.model.AccountCreateRequest;
import com.github.alpert.service.AccountService;
import com.github.alpert.service.TransactionService;
import io.netty.handler.codec.http.HttpResponseStatus;

@Path
public class AccountController {
    @Inject
    private AccountService accountService;
    @Inject
    private TransactionService transactionService;

    @GetRoute("account/:accountId")
    @JSON
    public RestResponse<?> account(@PathParam Long accountId) {
        final Account account = accountService.get(accountId);
        if (account == null) {
            return RestResponse
                .fail(HttpResponseStatus.NOT_FOUND.code(),
                    "Could not find account with id: " + accountId);
        }
        return RestResponse.ok(account).code(HttpResponseStatus.CREATED.code());
    }

    @PostRoute("account")
    @JSON
    public RestResponse<?> createAccount(@BodyParam AccountCreateRequest accountCreateRequest) {
        return RestResponse.ok(accountService.save(accountCreateRequest));
    }
}
