package com.github.alpert.controller;

import com.blade.ioc.annotation.Inject;
import com.blade.mvc.annotation.BodyParam;
import com.blade.mvc.annotation.JSON;
import com.blade.mvc.annotation.Path;
import com.blade.mvc.annotation.PostRoute;
import com.blade.mvc.ui.RestResponse;
import com.github.alpert.model.Deposit;
import com.github.alpert.model.Transfer;
import com.github.alpert.model.Withdraw;
import com.github.alpert.service.AccountService;
import com.github.alpert.service.TransactionService;
import com.github.alpert.transaction.DepositTransaction;
import com.github.alpert.transaction.Transaction;
import com.github.alpert.transaction.TransferTransaction;
import com.github.alpert.transaction.WithdrawTransaction;
import io.netty.handler.codec.http.HttpResponseStatus;

@Path
public class TransferController {

    @Inject
    private AccountService accountService;
    @Inject
    private TransactionService transactionService;

    @PostRoute("/transfer")
    @JSON
    public RestResponse<?> transfer(@BodyParam Transfer transfer) {
        if (transfer.getSenderId().equals(transfer.getReceiverId())) {
            return RestResponse
                .fail(HttpResponseStatus.BAD_REQUEST.code(),
                    "You cannot transfer money to same account.");
        }

        final boolean senderExist = accountService.isExist(transfer.getSenderId());
        final boolean receiverExist = accountService.isExist(transfer.getReceiverId());
        if (!senderExist || !receiverExist) {
            return RestResponse
                .fail(HttpResponseStatus.NOT_FOUND.code(),
                    "Could not found account with id: " + (senderExist ? transfer.getSenderId() :
                        transfer.getReceiverId()));
        }

        final Transaction transactionEvent =
            new TransferTransaction(transfer.getSenderId(), transfer.getReceiverId(), transfer.getAmount());
        transactionService.process(transactionEvent);

        return RestResponse.ok(transactionEvent);
    }

    @PostRoute("/withdraw")
    @JSON
    public RestResponse<?> withdraw(@BodyParam Withdraw withdraw) {
        if (!accountService.isExist(withdraw.getAccountId())) {
            return RestResponse
                .fail(HttpResponseStatus.NOT_FOUND.code(),
                    "Could not found account with id: " + withdraw.getAccountId());
        }

        final Transaction transactionEvent =
            new WithdrawTransaction(withdraw.getAccountId(), withdraw.getAmount());
        transactionService.process(transactionEvent);

        return RestResponse.ok(transactionEvent);
    }

    @PostRoute("/deposit")
    @JSON
    public RestResponse<?> deposit(@BodyParam Deposit deposit) {
        if (!accountService.isExist(deposit.getAccountId())) {
            return RestResponse
                .fail(HttpResponseStatus.NOT_FOUND.code(),
                    "Could not found account with id: " + deposit.getAccountId());
        }

        final Transaction transactionEvent =
            new DepositTransaction(deposit.getAccountId(), deposit.getAmount());
        transactionService.process(transactionEvent);

        return RestResponse.ok(transactionEvent);
    }
}