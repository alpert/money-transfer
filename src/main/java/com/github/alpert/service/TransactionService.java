package com.github.alpert.service;

import com.blade.ioc.annotation.Bean;
import com.blade.ioc.annotation.Inject;
import com.github.alpert.persistence.AccountStore;
import com.github.alpert.persistence.TransactionStore;
import com.github.alpert.transaction.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Bean
public class TransactionService {
    private static final Logger log = LoggerFactory.getLogger(TransactionService.class);


    @Inject
    private TransactionStore transactionStore;
    @Inject
    private AccountStore accountStore;

    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Transaction get(final String transactionId) {
        return transactionStore.getById(transactionId);
    }

    public void process(final Transaction transaction) {
        executorService.schedule(() -> {
            log.info("Executing task.");
            transactionStore.save(transaction);
            transaction.process(accountStore);
            transactionStore.update(transaction);
            log.info("Executed task.");
        }, 0, TimeUnit.MILLISECONDS);
    }
}
