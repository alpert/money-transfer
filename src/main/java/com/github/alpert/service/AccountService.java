package com.github.alpert.service;

import com.blade.ioc.annotation.Bean;
import com.blade.ioc.annotation.Inject;
import com.github.alpert.domain.Account;
import com.github.alpert.model.AccountCreateRequest;
import com.github.alpert.persistence.AccountStore;

@Bean
public class AccountService {

    @Inject
    private AccountStore accountStore;

    public Account save(final AccountCreateRequest accountCreateRequest) {
        final Account account =
            Account.builder()
                .owner(accountCreateRequest.getOwner())
                .balance(accountCreateRequest.getBalance())
                .build();
        return accountStore.save(account);
    }

    public Account get(final Long accountId) {
        return accountStore.get(accountId);
    }

    public boolean isExist(final Long accountId) {
        return accountStore.get(accountId) != null;
    }
}
