package com.github.alpert.transaction;

import com.github.alpert.domain.Account;
import com.github.alpert.domain.TransactionResult;
import com.github.alpert.domain.TransactionStatus;
import com.github.alpert.persistence.AccountStore;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.UUID;

public class TransferTransaction implements Transaction {
    @Getter
    private final String transactionId;
    @Getter
    private TransactionResult result;
    private final Long senderId;
    private final Long receiverId;
    private final BigDecimal amount;

    public TransferTransaction(final Long senderId, final Long receiverId, final BigDecimal amount) {
        this.transactionId = "TRX-" + UUID.randomUUID();
        this.result = TransactionResult.builder().status(TransactionStatus.RECEIVED).build();

        this.senderId = senderId;
        this.receiverId = receiverId;
        this.amount = amount;
    }

    @Override
    public void process(final AccountStore accountStore) {
        final Account sender = accountStore.get(senderId);
        final Account receiver = accountStore.get(receiverId);
        if (sender == null || receiver == null) {
            result.setStatus(TransactionStatus.FAILURE)
                .setMessage(String.format("invalid %s account", (sender == null ? "sender" : "receiver")));
            return;
        }

        if (sender.getBalance().compareTo(amount) < 0) {
            result.setStatus(TransactionStatus.FAILURE)
                .setMessage("insufficient funds");
            return;
        }

        sender.setBalance(sender.getBalance().subtract(amount));
        receiver.setBalance(receiver.getBalance().add(amount));

        accountStore.save(sender);
        accountStore.save(receiver);

        result.setStatus(TransactionStatus.SUCCESS);
    }
}
