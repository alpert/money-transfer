package com.github.alpert.transaction;

import com.github.alpert.domain.Account;
import com.github.alpert.domain.TransactionResult;
import com.github.alpert.domain.TransactionStatus;
import com.github.alpert.persistence.AccountStore;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.UUID;

public class WithdrawTransaction implements Transaction {
    private final Long accountId;
    private final BigDecimal amount;
    @Getter
    private final String transactionId;
    @Getter
    private TransactionResult result;

    public WithdrawTransaction(final Long accountId, final BigDecimal amount) {
        this.transactionId = "WTD-" + UUID.randomUUID();
        this.result = TransactionResult.builder().status(TransactionStatus.RECEIVED).build();

        this.accountId = accountId;
        this.amount = amount;
    }

    @Override
    public void process(final AccountStore accountStore) {
        final Account account = accountStore.get(accountId);
        if (account == null) {
            result.setStatus(TransactionStatus.FAILURE)
                .setMessage("invalid account");
            return;
        }

        if (account.getBalance().compareTo(amount) < 0) {
            result.setStatus(TransactionStatus.FAILURE)
                .setMessage("insufficient funds");
            return;
        }

        final BigDecimal value = account.getBalance().subtract(amount);
        account.setBalance(value);

        accountStore.save(account);

        result.setStatus(TransactionStatus.SUCCESS);
    }
}
