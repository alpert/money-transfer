package com.github.alpert.transaction;

import com.github.alpert.persistence.AccountStore;

public interface Transaction {
    String getTransactionId();

    void process(final AccountStore accountStore);

}
