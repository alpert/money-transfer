package com.github.alpert.transaction;

import com.github.alpert.domain.Account;
import com.github.alpert.domain.TransactionResult;
import com.github.alpert.domain.TransactionStatus;
import com.github.alpert.persistence.AccountStore;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.UUID;

public class DepositTransaction implements Transaction {
    @Getter
    private final String transactionId;
    @Getter
    private TransactionResult result;
    private final Long accountId;
    private final BigDecimal amount;

    public DepositTransaction(final Long accountId, final BigDecimal amount) {
        this.transactionId = "DEP-" + UUID.randomUUID();
        this.result = TransactionResult.builder().status(TransactionStatus.RECEIVED).build();

        this.accountId = accountId;
        this.amount = amount;
    }

    @Override
    public void process(final AccountStore accountStore) {
        final Account account = accountStore.get(accountId);
        if (account == null) {
            result.setStatus(TransactionStatus.FAILURE)
                .setMessage("invalid account");
            return;
        }
        final BigDecimal value = account.getBalance().add(amount);
        account.setBalance(value);

        accountStore.save(account);

        result.setStatus(TransactionStatus.SUCCESS);
    }
}
