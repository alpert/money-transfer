package com.github.alpert.transaction;

import com.blade.ioc.annotation.Inject;
import com.github.alpert.BaseTest;
import com.github.alpert.domain.Account;
import com.github.alpert.domain.TransactionStatus;
import com.github.alpert.persistence.AccountStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class WithdrawTransactionTest extends BaseTest {
    @Inject
    private AccountStore accountStore;

    @Before
    public void clear() {
        accountStore.clear();
    }

    @Test
    public void invalidAccountResultsFailure() {
        final Long nonExistingAccount = 1L;
        final BigDecimal amount = BigDecimal.TEN;
        final WithdrawTransaction withdrawTransaction = new WithdrawTransaction(nonExistingAccount, amount);

        withdrawTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.FAILURE, withdrawTransaction.getResult().getStatus());
        Assert.assertEquals("invalid account", withdrawTransaction.getResult().getMessage());
    }

    @Test
    public void insufficientFundsResultsFailure() {
        final Long accountId = 1L;
        final Account account = Account.builder()
            .accountId(accountId)
            .owner("Bob Marley")
            .balance(BigDecimal.ONE)
            .build();
        accountStore.save(account);

        final BigDecimal amount = BigDecimal.TEN;
        final WithdrawTransaction withdrawTransaction = new WithdrawTransaction(accountId, amount);

        withdrawTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.FAILURE, withdrawTransaction.getResult().getStatus());
        Assert.assertEquals("insufficient funds", withdrawTransaction.getResult().getMessage());
    }

    @Test
    public void validAccountResultsSuccess() {
        final Long accountId = 1L;
        final BigDecimal amount = BigDecimal.TEN;
        final Account account = Account.builder()
            .accountId(accountId)
            .owner("Bob Marley")
            .balance(amount)
            .build();
        accountStore.save(account);

        final WithdrawTransaction withdrawTransaction = new WithdrawTransaction(accountId, amount);

        withdrawTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.SUCCESS, withdrawTransaction.getResult().getStatus());
        Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
    }
}
