package com.github.alpert.transaction;

import com.blade.ioc.annotation.Inject;
import com.github.alpert.BaseTest;
import com.github.alpert.domain.Account;
import com.github.alpert.domain.TransactionStatus;
import com.github.alpert.persistence.AccountStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class TransferTransactionTest extends BaseTest {
    @Inject
    private AccountStore accountStore;

    @Before
    public void clear() {
        accountStore.clear();
    }

    @Test
    public void invalidSenderAccountResultsFailure() {
        final Account receiver = Account.builder()
            .accountId(1L)
            .owner("Bob Marley")
            .balance(BigDecimal.TEN)
            .build();
        accountStore.save(receiver);

        final Long nonExistingAccount = 2L;
        final TransferTransaction transferTransaction =
            new TransferTransaction(nonExistingAccount, receiver.getAccountId(), BigDecimal.TEN);

        transferTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.FAILURE, transferTransaction.getResult().getStatus());
        Assert.assertEquals("invalid sender account", transferTransaction.getResult().getMessage());
    }

    @Test
    public void invalidReceiverAccountResultsFailure() {
        final Account sender = Account.builder()
            .accountId(1L)
            .owner("Bob Marley")
            .balance(BigDecimal.TEN)
            .build();
        accountStore.save(sender);

        final Long nonExistingAccount = 2L;
        final TransferTransaction transferTransaction =
            new TransferTransaction(sender.getAccountId(), nonExistingAccount, BigDecimal.TEN);

        transferTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.FAILURE, transferTransaction.getResult().getStatus());
        Assert.assertEquals("invalid receiver account", transferTransaction.getResult().getMessage());
    }

    @Test
    public void insufficientFundsResultsFailure() {
        final Account sender = Account.builder()
            .accountId(1L)
            .owner("Bob Marley")
            .balance(BigDecimal.ONE)
            .build();
        final Account receiver = Account.builder()
            .accountId(2L)
            .owner("Bob Ros")
            .balance(BigDecimal.TEN)
            .build();
        accountStore.save(sender);
        accountStore.save(receiver);

        final TransferTransaction transferTransaction =
            new TransferTransaction(sender.getAccountId(), receiver.getAccountId(), BigDecimal.TEN);

        transferTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.FAILURE, transferTransaction.getResult().getStatus());
        Assert.assertEquals("insufficient funds", transferTransaction.getResult().getMessage());
    }

    @Test
    public void validAccountsAndFundsResultsSuccess() {
        final Account sender = Account.builder()
            .accountId(1L)
            .owner("Bob Marley")
            .balance(BigDecimal.TEN)
            .build();
        final Account receiver = Account.builder()
            .accountId(2L)
            .owner("Bob Ros")
            .balance(BigDecimal.ONE)
            .build();
        accountStore.save(sender);
        accountStore.save(receiver);

        final TransferTransaction transferTransaction =
            new TransferTransaction(sender.getAccountId(), receiver.getAccountId(), BigDecimal.TEN);

        transferTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.SUCCESS, transferTransaction.getResult().getStatus());
        Assert.assertEquals(BigDecimal.ZERO, sender.getBalance());
        Assert.assertEquals(BigDecimal.valueOf(11), receiver.getBalance());
    }
}
