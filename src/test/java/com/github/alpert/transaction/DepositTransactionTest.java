package com.github.alpert.transaction;

import com.blade.ioc.annotation.Inject;
import com.github.alpert.BaseTest;
import com.github.alpert.domain.Account;
import com.github.alpert.domain.TransactionStatus;
import com.github.alpert.persistence.AccountStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DepositTransactionTest extends BaseTest {
    @Inject
    private AccountStore accountStore;

    @Before
    public void clear() {
        accountStore.clear();
    }

    @Test
    public void invalidAccountResultsFailure() {
        final Long nonExistingAccount = 1L;
        final BigDecimal amount = BigDecimal.TEN;
        final DepositTransaction depositTransaction = new DepositTransaction(nonExistingAccount, amount);

        depositTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.FAILURE, depositTransaction.getResult().getStatus());
        Assert.assertEquals("invalid account", depositTransaction.getResult().getMessage());
    }

    @Test
    public void validAccountResultsSuccess() {
        final Long accountId = 1L;
        final BigDecimal amount = BigDecimal.TEN;
        final Account account = Account.builder()
            .accountId(accountId)
            .owner("Bob Marley")
            .balance(amount)
            .build();
        accountStore.save(account);

        final DepositTransaction depositTransaction = new DepositTransaction(accountId, amount);

        depositTransaction.process(accountStore);

        Assert.assertEquals(TransactionStatus.SUCCESS, depositTransaction.getResult().getStatus());
        Assert.assertEquals(BigDecimal.valueOf(20), account.getBalance());
    }
}
