package com.github.alpert;

import com.blade.test.BladeApplication;
import com.blade.test.BladeTestRunner;
import com.github.alpert.MoneyTransfer;
import org.junit.runner.RunWith;

@RunWith(BladeTestRunner.class)
@BladeApplication(MoneyTransfer.class)
public abstract class BaseTest {

}