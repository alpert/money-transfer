package com.github.alpert.controller;

import com.blade.ioc.annotation.Inject;
import com.github.alpert.BaseTest;
import com.github.alpert.domain.Account;
import com.github.alpert.persistence.AccountStore;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class AccountControllerTest extends BaseTest {
    @Inject
    private AccountStore accountStore;

    @Before
    public void clear() {
        accountStore.clear();
    }

    @Test
    public void notExistingUserReturns404() throws UnirestException {
        final HttpResponse<JsonNode> accountHttpResponse =
            Unirest.get("http://127.0.0.1:9000/account/1").asJson();

        Assert.assertEquals("404", accountHttpResponse.getBody().getObject().get("code").toString());
        Assert.assertEquals("Could not find account with id: 1",
            accountHttpResponse.getBody().getObject().get("msg").toString());
    }

    @Test
    public void returnExistingUser() throws UnirestException {
        final Account account = Account.builder()
            .accountId(1L)
            .owner("Bob Marley")
            .balance(BigDecimal.valueOf(200L))
            .build();
        accountStore.save(account);

        final HttpResponse<JsonNode> accountHttpResponse =
            Unirest.get("http://127.0.0.1:9000/account/1").asJson();

        Assert.assertEquals("{\"owner\":\"Bob Marley\",\"accountId\":1,\"balance\":\"200.00\"}",
            accountHttpResponse.getBody().getObject().get("payload").toString());
        Assert.assertEquals("200", accountHttpResponse.getBody().getObject().get("code").toString());
    }

    @Test
    public void createNewAccount() throws UnirestException {
        final HttpResponse<JsonNode> accountHttpResponse =
            Unirest.post("http://127.0.0.1:9000/account").body("{\"owner\": \"Bob Marley\",\"balance\": 200}").asJson();

        Assert.assertEquals("{\"owner\":\"Bob Marley\",\"accountId\":1,\"balance\":\"200.00\"}",
            accountHttpResponse.getBody().getObject().get("payload").toString());
    }
}
